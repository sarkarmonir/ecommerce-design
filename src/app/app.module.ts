import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';

import { CarouselModule, TooltipModule, RatingModule, TabsModule } from 'ngx-bootstrap';
import { HeaderComponent } from './partial/header/header.component';
import { FooterComponent } from './partial/footer/footer.component';
import { NewsletterComponent } from './partial/newsletter/newsletter.component';
import { HomeComponent } from './pages/home/home.component';
import { SliderComponent } from './partial/slider/slider.component';
import { BreadcrumbComponent } from './partial/breadcrumb/breadcrumb.component';
import { ProductsComponent } from './pages/products/products.component';
import { ProductDetailComponent } from './pages/product-detail/product-detail.component';
import { LoginComponent } from './pages/retailer/login/login.component';
import { RegisterComponent } from './pages/retailer/register/register.component';


const routes: Routes = [
  {path: '' , component: HomeComponent},
  {path: 'others' , component: BreadcrumbComponent},
  {path: 'shop' , component: ProductsComponent},
  {path: 'product-detail' , component: ProductDetailComponent},
  {path: 'login' , component: LoginComponent},
  {path: 'register' , component: RegisterComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    NewsletterComponent,
    HomeComponent,
    SliderComponent,
    BreadcrumbComponent,
    ProductsComponent,
    ProductDetailComponent,
    LoginComponent,
    RegisterComponent
  ],
  imports: [
    BrowserModule,
    CarouselModule.forRoot(),
    TooltipModule.forRoot(),
    RatingModule.forRoot(),
    TabsModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }